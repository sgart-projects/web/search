class Config {
	constructor() {}

	getThemeMode() {
		const themeModes = {
			'light': {
				name: 'Light',
				icon: 'light-mode'
			},
			'dark': {
				name: 'Dark',
				icon: 'dark-mode'
			},
			'auto': {
				name: 'Auto',
				icon: 'auto-mode',
				lightHour: '7',
				darkHour: '19'
			}
		}

		return themeModes;
	}

	getQuickSearchData() {
		const quickSearchData = {
			'r/': {
				urlPrefix: 'https://reddit.com/r/'
			},
			'w/': {
				urlPrefix: 'https://wikipedia.org/wiki/'
			},
			'u/': {
				urlPrefix: 'https://unsplash.com/s/photos/'
			},
			'a/': {
				urlPrefix: 'https://amazon.com/s?k='
			},
			'y/': {
				urlPrefix: 'https://youtube.com/results?search_query='
			},
			'g/': {
				urlPrefix: 'https://gitlab.com/search?q='
			},
			'd/': {
				urlPrefix: 'https://digitec.ch/en/search?q='
			},
			't/': {
				urlPrefix: 'https://www.thingiverse.com/search?q='
			}
		};

		return quickSearchData;
	}

	getSearchEngines() {

		const searchEngines = {
			'ecosia': {
				name: 'Ecosia',
				prefix: 'https://www.ecosia.org/search?q=',
				icon: 'ecosia'
			},
			'duckduckgo': {
				name: 'Duckduckgo',
				prefix: 'https://duckduckgo.com/?q=',
				icon: 'duckduckgo'
			},
			'google': {
				name: 'Google',
				prefix: 'https://www.google.com/search?q=',
				icon: 'google'
			}
		};

		return searchEngines;
	}

	getWebSites() {
		// Web menu
		// A list of websites that will be generated and put on the web menu
		const webSites = [
			{
				site: 'Reddit',
				icon: 'reddit',
				url: 'https://reddit.com/',
				category: 'social'
			},
			{
				site: 'Github',
				icon: 'github',
				url: 'https://github.com/',
				category: 'development'
			},
			{
				site: 'Gmail',
				icon: 'gmail',
				url: 'https://mail.google.com/',
				category: 'social'
			},
			{
				site: 'Youtube',
				icon: 'youtube',
				url: 'https://youtube.com/',
				category: 'media'
			},
			{
				site: 'Google Drive',
				icon: 'gdrive',
				url: 'https://drive.google.com/',
				category: 'cloud'
			},
			{
				site: 'Twitter',
				icon: 'twitter',
				url: 'https://twitter.com/',
				category: 'social'
			},
			{
				site: 'Instagram',
				icon: 'instagram',
				url: 'https://instagram.com/',
				category: 'social'
			},
			{
				site: 'Bitbucket',
				icon: 'bitbucket',
				url: 'https://bitbucket.org/',
				category: 'development'
			},
			{
				site: 'Gitlab',
				icon: 'gitlab',
				url: 'https://gitlab.com/',
				category: 'development'
			},
			{
				site: 'Deviantart',
				icon: 'deviantart',
				url: 'https://deviantart.com/',
				category: 'design'
			},
			{
				site: 'Duckduckgo',
				icon: 'duckduckgo',
				url: 'https://duckduckgo.com/',
				category: 'search Engine'
			},
			{
				site: 'Ecosia',
				icon: 'ecosia',
				url: 'https://ecosia.org/',
				category: 'search Engine'
			},
			{
				site: 'Google',
				icon: 'google',
				url: 'https://google.com/',
				category: 'search Engine'
			},
			{
				site: 'Wikipedia',
				icon: 'wikipedia',
				url: 'https://wikipedia.org/',
				category: 'information'
			},
			{
				site: 'Unsplash',
				icon: 'unsplash',
				url: 'https://unsplash.com/',
				category: 'design'
			},
			{
				site: 'Twitch',
				icon: 'twitch',
				url: 'https://twitch.tv/',
				category: 'media'
			},
			{
				site: 'Material.io',
				icon: 'materialio',
				url: 'https://material.io/',
				category: 'design'
			},
			{
				site: 'Netflix',
				icon: 'netflix',
				url: 'https://netflix.com/',
				category: 'media'
			},
			{
				site: 'Office 365',
				icon: 'office365',
				url: 'https://office.com/',
				category: 'information'
			},
			{
				site: 'Discord',
				icon: 'discord',
				url: 'https://discord.com/',
				category: 'social'
			},
			{
				site: 'Spotify',
				icon: 'spotify',
				url: 'https://spotify.com/',
				category: 'media'
			},
			{
				site: 'JSFiddle',
				icon: 'jsfiddle',
				url: 'https://jsfiddle.net/',
				category: 'development'
			},
			{
				site: 'Figma',
				icon: 'figma',
				url: 'https://figma.com/',
				category: 'design'
			},
			{
				site: 'Stackoverflow',
				icon: 'stackoverflow',
				url: 'https://stackoverflow.com/',
				category: 'development'
			},
			{
				site: 'Stackexchange',
				icon: 'stackexchange',
				url: 'https://stackexchange.com/',
				category: 'development'
			},
			{
				site: 'Superuser',
				icon: 'superuser',
				url: 'https://superuser.com/',
				category: 'development'
			},
			{
				site: 'Messenger',
				icon: 'messenger',
				url: 'https://messenger.com/',
				category: 'social'
			},
			{
				site: 'Icons8',
				icon: 'icons8',
				url: 'https://icons8.com/',
				category: 'design'
			},
			{
				site: 'Markdown Cheatsheet',
				icon: 'markdown',
				url: 'https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet/',
				category: 'development'
			},
			{
				site: 'Interneting is Hard',
				icon: 'interneting-is-hard',
				url: 'https://internetingishard.com/',
				category: 'development'
			},
			{
				site: 'Keycode',
				icon: 'keycode',
				url: 'https://keycode.info/',
				category: 'development'
			},
			{
				site: 'Flaticon',
				icon: 'flaticon',
				url: 'https://flaticon.com/',
				category: 'design'
			},
			{
				site: 'Wikimedia Commons',
				icon: 'commons',
				url: 'https://commons.wikimedia.org/',
				category: 'design'
			},
			{
				site: 'MDN Web Docs',
				icon: 'mdn',
				url: 'https://developer.mozilla.org/',
				category: 'development'
			},
			{
				site: 'Home Assistant',
				icon: 'homeassistant',
				url: 'https://home.1samuel3.com/',
				category: 'smart Home'
			},
			{
				site: 'Nextcloud',
				icon: 'nextcloud',
				url: 'https://cloud.1samuel3.com/',
				category: 'cloud'
			},
			{
				site: 'Portainer',
				icon: 'docker',
				url: 'https://portainer.1samuel3.com/',
				category: 'development'
			},
			{
				site: 'Octoprint',
				icon: '3dprinter',
				url: 'https://oktoprint.1samuel3.com/',
				category: '3D Printing'
			},
			{
				site: 'Thingiverse',
				icon: 'thingiverse',
				url: 'https://thingiverse.com/',
				category: '3D Printing'
			},
			{
				site: 'Bitwarden',
				icon: 'bitwarden',
				url: 'https://bitwarden.com/',
				category: 'security'
			},
			{
				site: 'Icon-Icons',
				icon: 'iconicons',
				url: 'https://icon-icons.com/',
				category: 'design'
			},
			{
				site: 'Nginx Docs',
				icon: 'nginx',
				url: 'https://docs.nginx.com/nginx/',
				category: 'development'
			},
			{
				site: 'discord.py Docs',
				icon: 'discorddevbadge',
				url: 'https://discordpy.readthedocs.io/en/latest/',
				category: 'development'
			},
			{
				site: 'Threema',
				icon: 'threema',
				url: 'https://web.threema.ch',
				category: 'social'
			},
			{
				site: 'Leo',
				icon: 'leo',
				url: 'https://dict.leo.org',
				category: 'information'
			},
			{
				site: 'JetBrains',
				icon: 'jetbrains',
				url: 'https://jetbrains.com',
				category: 'development'
			},
			{
				site: 'Digitec',
				icon: 'digitec',
				url: 'https://digitec.ch',
				category: 'shopping'
			},
			{
				site: 'Amazon',
				icon: 'amazon',
				url: 'https://amazon.com',
				category: 'shopping'
			},
			{
				site: 'V-Zug',
				icon: 'vzug',
				url: 'https://vzug.com',
				category: 'shopping'
			},
			{
				site: 'Outlook',
				icon: 'outlook',
				url: 'https://outlook.office.com',
				category: 'social'
			},
			{
				site: 'Pixabay',
				icon: 'pixabay',
				url: 'https://pixabay.com',
				category: 'design'
			},
			{
				site: 'OneDrive',
				icon: 'onedrive',
				url: 'https://onedrive.microsoft.com',
				category: 'cloud'
			}
		];

		return webSites;
	}

	getPanelSites() {
		// Panel
		// A list of websites that will be generated and put on the Panel
		const panelSites = [
			{
				site: 'HomeAssistant',
				icon: 'homeassistant',
				url: 'https://home.1samuel3.com/'
			},
			{
				site: 'Cloud',
				icon: 'nextcloud',
				url: 'https://cloud.1samuel3.com/'
			},
			{
				site: 'Portainer',
				icon: 'docker',
				url: 'https://portainer.1samuel3.com/'
			},
			{
				site: 'Youtube',
				icon: 'youtube',
				url: 'https://youtube.com/'
			},
			{
				site: 'Gitlab',
				icon: 'gitlab',
				url: 'https://gitlab.com/'
			},
			{
				site: 'Digitec',
				icon: 'digitec',
				url: 'https://digitec.ch/'
			}
		];

		return panelSites;
	}
}
